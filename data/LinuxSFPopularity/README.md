## Data Information

* License: [Creative Commons 3.0 By-ShareAlike](https://creativecommons.org/licenses/by-sa/3.0/)
* Data Source: [Stack Exchange Data](https://data.stackexchange.com)
* Data Query: [Permalink](https://data.stackexchange.com/stackoverflow/query/608257)
* Gathered By: [Mike Lloyd](https://data.stackexchange.com/users/25055/mxplusb)